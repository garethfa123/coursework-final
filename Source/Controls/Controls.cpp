//
//  Controls.cpp
//  JuceBasicAudio
//
//  Created by Gareth on 22/01/2018.
//
//

#include "Controls.hpp"

Controls::Controls()
{
    addAndMakeVisible(speedSlider);                                             /** Speed Slider. */
    speedSlider.setRange(1, 10, 1);
    speedSlider.addListener (this);
    
    addAndMakeVisible(gainSlider);                                              /** Gain Slider. */
    gainSlider.addListener (this);
    gainSlider.setRange(0, 10);
    gainSlider.setValue(4);
    gainSlider.setSliderStyle(juce::Slider::LinearVertical);
    
    addAndMakeVisible(submitButton);                                            /** Submit Button. */
    submitButton.addListener (this);
    submitButton.setButtonText ("Submit");
    
    addAndMakeVisible(startButton);                                             /** Start Button. */
    startButton.addListener (this);
    startButton.setButtonText ("Start");
    
    resetButton.addListener (this);                                             /** Reset Button. */
    resetButton.setButtonText ("Reset");
    
    addAndMakeVisible(answerBox);                                               /** Answer box. */
    
    addAndMakeVisible(modeSelector);                                        /** Mode Selector. */
    modeSelector.addItem ("Sandbox", 1);
    modeSelector.addItem ("Champion", 2);
    modeSelector.addListener(this);
    modeSelector.setTextWhenNothingSelected("Select Mode");
    
    
    
    addAndMakeVisible(prevAnswer);                                                 /** Previous answer box. */
    prevAnswer.setReadOnly(true);
    
    correctAnswer.setReadOnly(true);                                                /** Correct answer box. */

}

Controls::~Controls()
{
    
}

void Controls::resized()
{
    speedSlider.setBounds (50,20, 983, 40);                                     /** Setting bounds. */
    gainSlider.setBounds (1240,250, 30, 250);
    submitButton.setBounds (1120,400, 100, 100);
    startButton.setBounds (1120,200, 100, 100);
    resetButton.setBounds (510,375, 100, 100);
    answerBox.setBounds ( 1120, 350, 100,50);
    prevAnswer.setBounds ( 1120, 100, 100,50);
    correctAnswer.setBounds ( 644, 327, 30,30);
    modeSelector.setBounds(1120, 150, 100, 50);
}

void Controls::sliderValueChanged (Slider* slider)
{

}

void Controls::buttonClicked (Button* button)
{
    if (button == &startButton)                                             /** Starts game. */
    {
        start = 1;
        makeNoteName();
        resetButton.setVisible(false);
        correctAnswer.setVisible(false);
    }
    
    if (button == &submitButton)                                            /** Submits answer. */
    {
        prevAnswer.setText(answerBox.getText());
        makeNoteName();
        if (answerBox.getTextValue() == noteName)                           /** Sees is answer is correct. */
        {
            correct = 1;
            answerBox.clear();
        }
        
        else
        {
            correct = 0;
            answerBox.clear();
        }
    }
    
    if (button == &resetButton)                                             /** Resets game. */
    {
        reset = 1;
        prevAnswer.clear();
        resetButton.setVisible(false);
        correctAnswer.setVisible(false);
        repaint();
    }
}

void Controls::comboBoxChanged(ComboBox* combobox)
{
    if (modeSelector.getText() == "Sandbox")                            /** Sets game mode to either sandbox or champion. */
    {
        comboMode = 1;
    }
    else if (modeSelector.getText() == "Champion")
    {
        comboMode = 2;
    }
    else                                                                /** Defaults to sandbox. */
    {
        comboMode = 1;
    }
}

void Controls::showReset()
{                                                                                      /** Display correct answer. */
    correctAnswer.setText(noteName);
    addAndMakeVisible(correctAnswer);
    addAndMakeVisible(resetButton);                                 /** Makes reset/correct answer appear. */
}

void Controls::makeNoteName()
{
    int pitchClass = currentNote % 12;                              /** Turns note number into note name. */
    int octave = (currentNote / 12) - 2;
    const StringArray notes = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
    noteName = notes[pitchClass] + String (octave);
}

int Controls::getSliderValue()                                      /** Returns speed slider value. */
{
    return speedSlider.getValue();
}

int Controls::setSliderValue(int score)                             /** Sets speed slider value. */
{
    speedSlider.setValue(score);
}

int Controls::getStart()                                            /** returns start value. */
{
    return start;
}

void Controls::setStart(int off)                                    /** sets start value. */
{
    start = off;
}

int Controls::getGainSlider()                                       /** returns gain slider value. */
{
    return gainSlider.getValue();
}

int Controls::getResetVal()                                         /** returns reset value. */
{
    return reset;
}

int Controls::getResult()                                           /** returns value of correct. */
{
    return correct;
}

void Controls::setCorrect(int zero)                                 /** Sets value of correct. */
{
    correct = zero;
}

void Controls::clearBoxes()                                         /** Clears answer boxes. */
{
    answerBox.clear();
    prevAnswer.clear();
}

void Controls::setCorrectAnswer(int note)                           /** Sets note number. @param note number */
{
    currentNote = note;
}

int Controls::getMode()                                             /** returns game mode value. @return game mode*/
{
    return comboMode;
}
