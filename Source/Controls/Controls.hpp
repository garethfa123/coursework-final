//
//  Controls.hpp
//  JuceBasicAudio
//
//  Created by Gareth on 22/01/2018.
//
//

#ifndef Controls_hpp
#define Controls_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"

/** Class containing buttons, sliders, comboboxes etc. This class contains all the main controls for the application, with their values all sent to the GameInterface class @see GameInterface. */

class Controls   : public Component,
                   public Slider::Listener,
                   public Button::Listener,
                   public ComboBox::Listener

{
public:
    //==============================================================================
    /** Constructor */
    Controls();
    
    
    /** Destructor */
    ~Controls();
    
    int getSliderValue();
    
    
    int setSliderValue(int score);
   
    
    int getStart();
   
    
    void setStart(int off);
    
    
    int getGainSlider();
    
    
    void showReset();
    
    int getResetVal();
    
    
    int getResult();
    
    
    void setCorrect(int zero);
    
    
    void clearBoxes();
    
    
    void setCorrectAnswer(int note);
    
    
    int getChannel();
    
    
    int getMode();
    
    
    void makeNoteName();
    
    void resized() override;
    void sliderValueChanged(Slider* slider) override;
    void buttonClicked (Button* button) override;
    void comboBoxChanged (ComboBox* combobox) override;
private:
Slider speedSlider;
Slider gainSlider;
TextButton submitButton;
TextButton startButton;
TextButton resetButton;
TextEditor answerBox;
TextEditor prevAnswer;
TextEditor correctAnswer;

ComboBox modeSelector;
    int currentNote;
    int correct = 0;
    int comboChannel = 1, comboMode = 1;
    int start = 0;
    int reset = 0;
    String noteName;
};

#endif /* Controls_hpp */
