/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"


/** Class containing all audio processes */

class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback


{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    void setGain (float newGain);
    
    int getNoteNum();
    
    
    int getGuessNoteNum();
    
    
    void setNoteZero(int zero);
    
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    void audioDeviceStopped() override;
    void handleIncomingMidiMessage(MidiInput*, const MidiMessage);
    void setNote();
    void playNote();
private:
    AudioDeviceManager audioDeviceManager;
    Atomic<float> gain;
    int noteNum;
    int inputNote;
    Atomic<float> frequency;
    float phasePosition;
    float sampleRate;
};

#endif  // AUDIO_H_INCLUDED
