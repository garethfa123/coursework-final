/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    audioDeviceManager.setMidiInputEnabled("Impulse  Impulse", true);
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    inputNote = message.getNoteNumber();                /** gets note number of key pressed **/
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    
    float Position;
    const float twoPi = 2 * M_PI;                                       /** Makes sine wave from frequency **/
    float phaseIncrement = (float)(twoPi * frequency.get())/sampleRate;
    
    while(numSamples--)
    {
        phasePosition = phasePosition + phaseIncrement;
        
        if (phasePosition > twoPi)
        {
            phasePosition = phasePosition - twoPi;
        }
        
        
        Position = sin(phasePosition);
        
        *outL = Position * (gain.get() / 10);                           /** outputs sound at level of gain slider **/
        *outR = Position * (gain.get() / 10);
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    phasePosition = 0.f;                                                /** Prepares for start **/
    sampleRate = device->getCurrentSampleRate();
    srand(time(NULL));                                                  /** Makes note order random each time **/
    setNote();
}

void Audio::audioDeviceStopped()
{
    
}

void Audio::setNote()
{
    noteNum = rand() % 36 + 48;                                         /** Random note number generator. **/
    frequency.set(440.f * std::powf(2, (noteNum - 69.0)/12.0));         /** Sets frequency. **/
}

void Audio::setGain (float newGain)                                     /** Sets Gain. @param gain slider value **/
{
    gain = newGain;
}

int Audio::getNoteNum()                                                 /** Returns note number.
                                                                         @return returns current note number
                                                                         */
{
    return noteNum;
}

int Audio::getGuessNoteNum()                                            /** Returns midi key note number.
                                                                         @return returns midi key input number
                                                                         */
{
    return inputNote;
}

void Audio::setNoteZero(int zero)                                       /** Sets frequency/note number to 0.
                                                                         @param always a 0 for note to stop playing
                                                                         */
{
    noteNum = 0;
    frequency.set(0);
}
