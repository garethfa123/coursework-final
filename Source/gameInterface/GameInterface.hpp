//
//  GameInterface.hpp
//  JuceBasicAudio
//
//  Created by Gareth on 22/01/2018.
//
//

#ifndef GameInterface_hpp
#define GameInterface_hpp

#include <stdio.h>
#include "../ball/Ball.hpp"
#include "../bumper/Bumper.hpp"
#include "../Controls/Controls.hpp"
#include "../JuceLibraryCode/JuceHeader.h"


/** Class containing everything to do with the interface. This class is the main class of the application, controlling the variables for the ball and bumper classes, while also controlling the audio class. */

class GameInterface   : public Timer,
                        public Component

{
public:
    //==============================================================================
    /** Constructor */
    GameInterface (Audio& a);
    
    /** Destructor */
    ~GameInterface();
    
    
    void paint (Graphics& g) override;
    void timerCallback() override;
    void resized() override;
    void yPixelNo();
    void reset();
    void answer();
    
private:
    Ball ball;
    Controls controls;
    Bumper bumper;
    Audio& audio;
    int forwards = 1, backwards = 0, up = 0, down = 0;
    int pixelNo = 0;
    int prevScore = 0;
    int resetGame = 0;
    int score = 0;
    int highscore = 0;
    int startNote = 0;
    String currentNote;
};

#endif /* GameInterface_hpp */
