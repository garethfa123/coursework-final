//
//  GameInterface.cpp
//  JuceBasicAudio
//
//  Created by Gareth on 22/01/2018.
//
//

#include "GameInterface.hpp"

GameInterface::GameInterface (Audio& a) : audio (a)
{
    addAndMakeVisible(ball);                            /** Makes components visible **/
    addAndMakeVisible(bumper);
    addAndMakeVisible(controls);
    startTimer(25);                                     /** Starts timer **/
    
    if (ball.getBallY() < pixelNo)                      /** sets ball to go down if ball is higher than destination **/
    {
        up = 0;
        down = 1;
    }
    
    if (ball.getBallY() > pixelNo)                      /** sets ball to go up if ball is lower than destination **/
    {
        up = 1;
        down = 0;
    }
}

GameInterface::~GameInterface()                         //Destructor
{
    
}

void GameInterface::paint (Graphics &g)
{
    g.setColour(Colours::lightblue);                                          /** Display Area **/
    g.fillRoundedRectangle(40,60,1000,728,10);
    
    g.setColour(Colours::white);                                              /** Display Lines **/
    g.fillRect(40, 414, 1000, 20);
    g.fillRect(535, 60, 10, 728);
    
    g.setColour(Colours::black);
    g.setFont(30);
    g.drawSingleLineText((String::formatted ("Score: %d", score)), 1120, 600);          /** Display all text **/
    g.drawSingleLineText((String::formatted ("Highscore: %d", highscore)), 1120, 630);
    g.drawSingleLineText(("Ear-Training Pong"), 432, 25);
    g.setFont(20);
    g.drawSingleLineText(("Speed Slider"), 50, 22);
    g.drawSingleLineText(("Volume"), 1225, 250);
    g.drawSingleLineText(("Current Answer:"), 1105, 95);
    g.drawSingleLineText(("Type Answer:"), 1115, 340);
    g.setFont(12);
    int pCounter = 780;
    
    for (int count = 2; count < 5; count ++)                                        /** Display note names on left side **/
    {
        g.drawSingleLineText((String::formatted ("C%d", count)), 15, pCounter);
        g.drawSingleLineText((String::formatted ("C#%d", count)), 15, pCounter - 20);
        g.drawSingleLineText((String::formatted ("D%d", count)), 15, pCounter - 40);
        g.drawSingleLineText((String::formatted ("D#%d", count)), 15, pCounter - 60);
        g.drawSingleLineText((String::formatted ("E%d", count)), 15, pCounter - 80);
        g.drawSingleLineText((String::formatted ("F%d", count)), 15, pCounter - 100);
        g.drawSingleLineText((String::formatted ("F#%d", count)), 15, pCounter - 120);
        g.drawSingleLineText((String::formatted ("G%d", count)), 15, pCounter - 140);
        g.drawSingleLineText((String::formatted ("G#%d", count)), 15, pCounter - 160);
        g.drawSingleLineText((String::formatted ("A%d", count)), 15, pCounter - 180);
        g.drawSingleLineText((String::formatted ("A#%d", count)), 15, pCounter - 200);
        g.drawSingleLineText((String::formatted ("B%d", count)), 15, pCounter - 220);
        
        pCounter = pCounter - 240;
    }
    
    pCounter = 780;
    
    for (int count = 2; count < 5; count ++)                                       /** Display note names on right side **/
    {
        g.drawSingleLineText((String::formatted ("C%d", count)), 1043, pCounter);
        g.drawSingleLineText((String::formatted ("C#%d", count)), 1043, pCounter - 20);
        g.drawSingleLineText((String::formatted ("D%d", count)), 1043, pCounter - 40);
        g.drawSingleLineText((String::formatted ("D#%d", count)), 1043, pCounter - 60);
        g.drawSingleLineText((String::formatted ("E%d", count)), 1043, pCounter - 80);
        g.drawSingleLineText((String::formatted ("F%d", count)), 1043, pCounter - 100);
        g.drawSingleLineText((String::formatted ("F#%d", count)), 1043, pCounter - 120);
        g.drawSingleLineText((String::formatted ("G%d", count)), 1043, pCounter - 140);
        g.drawSingleLineText((String::formatted ("G#%d", count)), 1043, pCounter - 160);
        g.drawSingleLineText((String::formatted ("A%d", count)), 1043, pCounter - 180);
        g.drawSingleLineText((String::formatted ("A#%d", count)), 1043, pCounter - 200);
        g.drawSingleLineText((String::formatted ("B%d", count)), 1043, pCounter - 220);
        
        pCounter = pCounter - 240;
    }
    
    if (resetGame == 1)                                                         /** Paints if reset has been triggered **/
    {
        g.setFont(50);
        g.drawSingleLineText((String::formatted ("Game Over!")), 450, 300);             /** Display game over text **/
        g.setFont(30);
        g.drawSingleLineText((String::formatted ("The note was: ")), 470, 350);         /** Display correct answer text **/
        
        if (score > highscore)                                            /** Display highscore text if new highscore **/
        {
            g.setFont(50);
            g.drawSingleLineText((String::formatted ("New Highscore!")), 410, 520);
            highscore = score;
        }
        
        resetGame = 0;                                                    /** Finalises game reset **/
        score = 0;
    }
    
    if (controls.getResult() == 1)                                     /** Display correct message if answer is correct **/
    {
        g.setFont(50);
        g.drawSingleLineText((String::formatted ("Correct!")), 450, 300);
    }
}

void GameInterface::resized()
{
    ball.setBounds(getLocalBounds());                                   /** Sets Bounds **/
    bumper.setBounds(getLocalBounds());
    controls.setBounds(getLocalBounds());
}

void GameInterface::timerCallback()                                     /** Timer callback. This function is called 25 times                                       
                                                                         *  a second, controlling the majority of the application. Anything that needs to happen while the game is running happens through this function. @see Audio @see Controls */
{
    if (controls.getStart() == 1)                                       /** If the game has been reset, set a new note */
    {
        if (startNote == 1)
        {
            audio.setNote();
            startNote = 0;
        }
        
        answer();                                                       /** check midi keyboard input */
        audio.setGain(controls.getGainSlider());                        /** set gain */
        yPixelNo();                                                     /** work out pixel no */
        
        
        if (controls.getMode() == 2)                                    /** If champion mode is selected, set speedslider to
                                                                         go up by one everytime the score reaches a multiple of three. */
        {
            if (score % 3 == 0 && prevScore != score)
            {
                controls.setSliderValue(controls.getSliderValue() + 1);
                prevScore = score;
                
            }
        }

        controls.setCorrectAnswer(audio.getNoteNum());
        controls.makeNoteName();
        
        
        if (ball.getBallY() < pixelNo)                      /** sets ball to go down if ball is higher than destination **/
        {
            up = 0;
            down = 1;
        }
        
        if (ball.getBallY() > pixelNo)                      /** sets ball to go up if ball is lower than destination **/
        {
            up = 1;
            down = 0;
        }
        
        if (ball.getBallX() <= 64 && controls.getResult() == 1)                 /** If correct and ball is on the left, give new freq and start moving right **/
        {
            forwards = 1;
            backwards = 0;
            controls.setCorrect(0);
            score++;
            audio.setNote();
            controls.clearBoxes();
        }
        
        if (ball.getBallX() >= 1010 && controls.getResult() == 1)             /** If correct and ball is on the right, give new freq and start moving left **/
        {
            forwards = 0;
            backwards = 1;
            controls.setCorrect(0);
            score++;
            audio.setNote();
            controls.clearBoxes();
        }
        
        if (ball.getBallX() > 1020 || ball.getBallX() < 50)                    /** If correct answer isn't given, reset **/
        {
            controls.setStart(0);
            reset();
        }
        
        
        if (controls.getResult() == 1)                                          /** Moves bumpers if answer is correct **/
        {
            if (bumper.getBumperY() < pixelNo)
            {
                bumper.setBumperY(bumper.getBumperY() + 10);
            }
            
            if (bumper.getBumperY() > pixelNo)
            {
                bumper.setBumperY(bumper.getBumperY() - 10);
            }
        }
        
        if (forwards == 1)                                                      /** moves ball right **/ //
        {
            ball.setBallX(ball.getBallX() + controls.getSliderValue());
        }
        
        if (backwards == 1)                                                     /** moves ball left **/ // 
        {
            ball.setBallX(ball.getBallX() - controls.getSliderValue());
        }
        
        
        
        
        if (forwards == 1 && down == 1 && pixelNo - ball.getBallY() >= 1010 - ball.getBallX() && ball.getBallY() < pixelNo)
        {
            ball.setBallY(ball.getBallY() + controls.getSliderValue());  /** Moves ball right and down **/
        }
        
        if (forwards == 1 && up == 1 && ball.getBallY() - pixelNo >= 1010 - ball.getBallX() && ball.getBallY() > pixelNo)
        {
           ball.setBallY(ball.getBallY() - controls.getSliderValue());   /** Moves ball right and up **/
        }
        
        if (backwards == 1 && down == 1 && pixelNo - ball.getBallY() >= ball.getBallX() - 60 && ball.getBallY() < pixelNo)
        {
            ball.setBallY(ball.getBallY() + controls.getSliderValue());  /** Moves ball left and down **/
        }
        
        if (backwards == 1 && up == 1 && ball.getBallY() - pixelNo >= ball.getBallX() - 60 && ball.getBallY() > pixelNo)
        {
            ball.setBallY(ball.getBallY() - controls.getSliderValue());  /** Moves ball left and up **/
        }
        
        
        
        
        if (up == 1 && bumper.getBumperY() - ball.getBallY() > 25 && (ball.getBallX() >= 1010 || ball.getBallX() <= 64))
        {
            controls.setCorrect(0);         /** If bumper hasn't moved in time, make answer incorrect **/
        }
        
        else if (down == 1 && ball.getBallY() - bumper.getBumperY() > 25 && (ball.getBallX() >= 1010 || ball.getBallX() <= 64))
        {
            controls.setCorrect(0);         /** If bumper hasn't moved in time, make answer incorrect **/
        }
        
        ball.drawBall();                    /** Draws ball/bumper **/
        bumper.drawBumper();
 
    }
    
}

void GameInterface::yPixelNo()                           /** Works out correct pixel. This function will be called to
                                                          *  work out the pixel number the ball must travel to.*/
{
    int pCounter = 780;                                 /** Works out pixel ball needs to go to for current note number **/
    for (int i = 48; i < 85; i++)
    {
        if (i == audio.getNoteNum())
        {
            pixelNo = pCounter;
        }
        pCounter = pCounter - 20;
    }
        
}

void GameInterface::reset()                         /** Resets game. This function will be called once the ball
                                                     *  crosses the left or right border of the game interface. 
                                                     *  Once executed, the game will be ready to reset */
{
    ball.setBallX(64);                              /** resets bumpers/ball */
    ball.setBallY(425);
    bumper.setBumperY(425);
    
    forwards = 1;
    backwards = 0;
    resetGame = 1;
    
    controls.showReset();
    
    audio.setNoteZero(0);                           /** Sets frequency to 0 @see Audio */
    
    startNote = 1;                                  /** Allows timer to set a new note for a new game */
    
    repaint();
    
    if (controls.getMode() == 2)                    /** if champion mode is selected, reset speed slider to 1 */
    {
        controls.setSliderValue(1);
    }
    
}

void GameInterface::answer()
{
    if (audio.getGuessNoteNum() == audio.getNoteNum())      /** Allows midi keyboard input to set correct answer **/
    {
        controls.setCorrect(1);
    }
    
}
