//
//  Bumper.hpp
//  JuceBasicAudio
//
//  Created by Gareth on 22/01/2018.
//
//

#ifndef Bumper_hpp
#define Bumper_hpp

#include <stdio.h>

#include "../JuceLibraryCode/JuceHeader.h"

/** Class containing the bumper variables. The X Value always stays the same for the bumper, but the Y value is controlled by the GameInterface class @see GameInterface */

class Bumper   : public Component

{
public:
    //==============================================================================
    /** Constructor */
    Bumper()
    {
        
    }
    /** Destructor */
    ~Bumper() {}
    
    int getBumperY();
    
    void setBumperY(int yBumper);
    
    
    void drawBumper();
    
    
    void paint (Graphics& g) override;
    
private:
    int bumperY = 420;
};

#endif /* Bumper_hpp */
