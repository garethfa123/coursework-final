//
//  Bumper.cpp
//  JuceBasicAudio
//
//  Created by Gareth on 22/01/2018.
//
//

#include "Bumper.hpp"

void Bumper::paint (Graphics &g)                                    /** Draws bumpers at correct X/Y coordinates **/
{
    g.fillRect(40, bumperY - 25, 20, 50);
    g.fillRect(1020, bumperY - 25, 20, 50);
}

int Bumper::getBumperY()                                            /** @return Returns bumper Y value **/
{
    return bumperY;
}

void Bumper::setBumperY(int yBumper)                                /** @param Sets bumper Y value **/
{
    bumperY = yBumper;
}

void Bumper::drawBumper()                                           /** Repaints **/
{
    repaint();
}
