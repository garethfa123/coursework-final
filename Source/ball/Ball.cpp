//
//  Ball.cpp
//  JuceBasicAudio
//
//  Created by Gareth on 22/01/2018.
//
//

#include "Ball.hpp"

void Ball::paint (Graphics &g)
{
   g.drawRoundedRectangle(ballX, ballY, 10, 10, 30, 10);                    /** Draws ball at correct X/Y coordinates **/
}

int Ball::getBallX()
{
    return ballX;                                                           /** @return Returns ball's X coord **/
}

int Ball::getBallY()                                                        /**  @return Returns ball's Y coord  **/
{
    return ballY;
}

void Ball::setBallX(int xVal)                                               /** @param Sets ball's X coord **/
{
    ballX = xVal;
}

void Ball::setBallY(int yVal)                                               /** @param Sets ball's Y coord **/
{
    ballY = yVal;
}

void Ball::drawBall()                                                       /** Repaints **/
{
    repaint();
}
