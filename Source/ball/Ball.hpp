//
//  Ball.hpp
//  JuceBasicAudio
//
//  Created by Gareth on 22/01/2018.
//
//

#ifndef Ball_hpp
#define Ball_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"


/** Class containing all the ball variables. The ball variables, X and Y, are controlled by the GameInterface class @see GameInterface */

class Ball   : public Component

{
public:
    //==============================================================================
    /** Constructor */
    Ball()
    {
        
    }
    /** Destructor */
    ~Ball() {}
    
    int getBallX();
    
    
    int getBallY();
    
    
    void setBallX(int xVal);
    
    
    void setBallY(int yVal);
    
    
    void drawBall();
    
    
    void paint (Graphics& g) override;
    
private:
    int ballX = 64;
    int ballY = 420;
};
#endif /* Ball_hpp */
